import pygame
import keyboard
from text import DisplayText

#initialize pygame
pygame.init()

#define screen
screen = pygame.display.set_mode((200, 100))

#define frame per second
fps = pygame.time.Clock()

#set loop condition
done = False

#initial value of count and sum
count = 0
sum = 0

#object of DisplayText
display_text = DisplayText()

#game loop
while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                    done = True
        if keyboard.read_key():
            count += 0.5

        pygame.display.set_caption("Key counter")
        screen.fill(pygame.Color( 255, 255, 215 ))
        pygame.display.flip()
        print(int(count))
        display_text.render_text(screen, "Today", (0, 0, 0), 70, 0)
        display_text.render_text(screen, str(int(count)), (0, 0, 0), 80, 30)
        pygame.display.update()
        with open('todayCount.txt', 'a') as file:
            file.write(str(int(count)))  
        fps.tick(60)
