# Key press counter
Key press counter is a Python project for dealing with key press counting.

## Requirement

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install some require package.
i.g.

*  Python
*  Pygame
*  keyboard

```bash
pip install pygame
```

## ***Importent  note***

Please make sure that you are in root permission.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
